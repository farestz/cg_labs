import os
import sys
import threading

import cv2
import numpy as np
import matplotlib.pyplot as plt
from PyQt5 import QtWidgets, QtGui

from skimage.measure import compare_ssim


class Metrics(QtWidgets.QWidget):
    def __init__(self):
        super(Metrics, self).__init__()
        self.filename = ''
        self.metrics_values = []

        self.orig_image = QtWidgets.QLabel()
        self.compress_image = QtWidgets.QLabel()

        # log
        self.log = QtWidgets.QTextEdit()
        self.log_label = QtWidgets.QLabel('Metrics')

        # open button
        self.filename_label = QtWidgets.QLabel(self.filename)
        self.open_btn = QtWidgets.QPushButton('Open image...')

        # jpeg compression
        self.compress_spinbox = QtWidgets.QSpinBox()
        self.compress_btn = QtWidgets.QPushButton('Go')
        self.compress_label = QtWidgets.QLabel('JPEG compression (%)')

        # plot
        self.plot_btn = QtWidgets.QPushButton('Generate plot')

        # plot metrics
        self.combobox_metrics = QtWidgets.QComboBox()

        self.ui()
        self.setup()
        self.widgets_setup()

    def ui(self):
        # up level layouts
        main_layout = QtWidgets.QGridLayout()
        image_layout = QtWidgets.QGridLayout()
        control_layout = QtWidgets.QVBoxLayout()
        control_buttons_layout = QtWidgets.QVBoxLayout()
        stat_layout = QtWidgets.QVBoxLayout()

        # sub-level layout
        open_button_layout = QtWidgets.QHBoxLayout()
        compression_layout = QtWidgets.QHBoxLayout()
        call_plot_layout = QtWidgets.QHBoxLayout()

        # image layout
        image_layout.addWidget(self.orig_image, 0, 0)
        image_layout.addWidget(self.compress_image, 0, 1)

        # open button layout
        open_button_layout.addWidget(self.filename_label)
        open_button_layout.addWidget(self.open_btn)

        # compression layout
        compression_layout.addWidget(self.compress_label)
        compression_layout.addWidget(self.compress_spinbox)
        compression_layout.addWidget(self.compress_btn)

        # plot layout
        call_plot_layout.addWidget(self.combobox_metrics)
        call_plot_layout.addWidget(self.plot_btn)

        # control buttons layout
        control_buttons_layout.addLayout(open_button_layout)
        control_buttons_layout.addLayout(compression_layout)

        # stat layout
        stat_layout.addWidget(self.log_label)
        stat_layout.addWidget(self.log)

        # control layout
        control_layout.addLayout(control_buttons_layout)
        control_layout.addLayout(stat_layout)
        control_layout.addLayout(call_plot_layout)

        # main layout
        main_layout.addLayout(image_layout, 0, 0)
        main_layout.addLayout(control_layout, 0, 1)

        self.setLayout(main_layout)
        self.showMaximized()
        self.setMinimumSize(1280, 580)
        self.setMaximumSize(1280, 580)

    def setup(self):
        self.plot_btn.clicked[bool].connect(lambda: self.plot(self.combobox_metrics.currentText()))
        self.open_btn.clicked[bool].connect(lambda: self.open_image())
        self.compress_btn.clicked[bool].connect(lambda: self.compress(self.compress_spinbox.value()))
        # self.compress_spinbox.valueChanged[int].connect(lambda value: self.compress(value))

    def widgets_setup(self):
        self.orig_image.setMinimumSize(400, 400)
        self.compress_image.setMinimumSize(400, 400)

        self.log.setReadOnly(True)

        self.compress_spinbox.setMinimum(0)
        self.compress_spinbox.setMaximum(100)
        self.compress_spinbox.setSingleStep(5)

        self.compress_btn.setEnabled(False)

        self.plot_btn.setEnabled(False)

        self.combobox_metrics.addItems(['MSE', 'PSNR', 'SSIM'])

    def open_image(self):
        self.filename = QtWidgets.QFileDialog.getOpenFileName(self,
                                                              'Open...',
                                                              os.path.expanduser('~'),
                                                              'Image files (*.jpg *.png)')[0]

        if not self.filename:
            return

        self.orig_image.setPixmap(QtGui.QPixmap(self.filename))
        self.compress_image.setPixmap(QtGui.QPixmap(self.filename))
        self.original_image = cv2.imread(self.filename)
        # self.original_image = cv2.resize(self.original_image, (0, 0), fx=0.5, fy=0.5)
        # cv2.imwrite('3.png', self.original_image)

        self.original_image = cv2.imencode('.jpg', self.original_image)
        self.original_image = cv2.imdecode(self.original_image, cv2.IMREAD_COLOR)

        # reset widgets
        self.log.clear()
        self.compress_spinbox.setValue(0)
        self.compress_btn.setEnabled(True)
        self.filename_label.setText(self.filename)

        # thread for generating values
        thread = threading.Thread(target=self.generate_metrics_values, args=())
        thread.daemon = True
        thread.start()

    def compress(self, value, update_image=True):
        new_filename = self.make_newfilename('{}'.format(value))
        cv2.imwrite(new_filename, self.original_image, [cv2.IMWRITE_JPEG_QUALITY, 100 - value])

        if update_image:
            self.compress_image.setPixmap(QtGui.QPixmap(new_filename))

        self.compr_image = cv2.imread(new_filename)
        os.remove(new_filename)

        if update_image:
            self.update_log(100 - value)

    def make_newfilename(self, name):
        new_filename = self.filename.split('/')
        new_filename[-1] = '.{0}{1}'.format(name.lower(), new_filename[-1])
        return '/'.join(new_filename)

    def update_log(self, qual):
        text = '''
        *******************************
        Quality: {quality}
        MSE: {mse}
        PSNR: {psnr}
        SSIM: {ssim}
        *******************************\n
        '''
        mse = self.mse()
        self.log.append(text.format(quality=qual,
                                    mse=mse,
                                    psnr=self.psnr(mse),
                                    ssim=compare_ssim(self.original_image,
                                                      self.compr_image,
                                                      win_size=None,
                                                      gradient=False,
                                                      multichannel=True)))

    def mse(self):
        error = np.sum((self.original_image.astype('float') - self.compr_image.astype('float')) ** 2)
        error /= float(self.original_image.shape[0] * self.compr_image.shape[1])
        return error / 3

    def psnr(self, mse):
        return 20 * np.log10(255 / np.sqrt(mse))

    def generate_metrics_values(self):
        self.metrics_values.clear()
        for qual in range(0, 105, 1):
            self.compress(qual, update_image=False)
            mse = self.mse()
            self.metrics_values.append({'MSE': mse,
                                        'PSNR': self.psnr(mse),
                                        'SSIM': compare_ssim(self.original_image,
                                                             self.compr_image,
                                                             win_size=None,
                                                             gradient=False,
                                                             multichannel=True)})
        self.plot_btn.setEnabled(True)

    def plot(self, metric_type):
        ax = [x for x in range(0, 105, 1)]
        ay = [x[metric_type] for x in self.metrics_values]

        plt.plot(ax, ay)
        plt.title('{} Metric'.format(metric_type))
        plt.xlabel('JPEG compression (%)')
        plt.ylabel('{} value'.format(metric_type))

        plt.show()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    ip = Metrics()
    sys.exit(app.exec_())
