import os
import sys

import skimage
from skimage import io
from skimage import filters

import numpy as np
from PyQt5 import QtWidgets, QtGui


class InstagramProcess(QtWidgets.QWidget):
    def __init__(self):
        super(InstagramProcess, self).__init__()

        self.filename = ''
        self.image = QtWidgets.QLabel()
        self.open_btn = QtWidgets.QPushButton('Open image...')

        self.combobox_filters = QtWidgets.QComboBox()
        self.combobox_filters.addItems(['Original', 'Sharped', 'Blurred', 'Gotham'])

        self.ui()
        self.setup()

    def ui(self):
        main_layout = QtWidgets.QVBoxLayout()
        button_layout = QtWidgets.QHBoxLayout()

        button_layout.addWidget(self.open_btn)
        button_layout.addWidget(self.combobox_filters)
        main_layout.addLayout(button_layout)
        main_layout.addWidget(self.image)

        self.setLayout(main_layout)
        self.show()
        self.setMinimumSize(500, 500)
        self.move(QtWidgets.QApplication.desktop().screen().rect().center() - self.rect().center())

    def setup(self):
        self.open_btn.clicked[bool].connect(lambda: self.open_image())
        self.combobox_filters.activated[str].connect(lambda text: self.activate_filters(text))

    def open_image(self):
        self.filename = QtWidgets.QFileDialog.getOpenFileName(self,
                                                              'Open...',
                                                              os.path.expanduser('~'),
                                                              'Image files (*.jpg *.png)')[0]
        self.image.setPixmap(QtGui.QPixmap(self.filename))

    def make_newfilename(self, name):
        new_filename = self.filename.split('/')
        new_filename[-1] = '.{0}{1}'.format(name.lower(), new_filename[-1])
        return '/'.join(new_filename)

    def update_image(self, image, name):
        new_filename = self.make_newfilename(name)
        io.imsave(new_filename, image)
        self.image.setPixmap(QtGui.QPixmap(new_filename))
        os.remove(new_filename)

    def split_image_into_channels(self, im):
        red, green, blue = im[:, :, 0], im[:, :, 1], im[:, :, 2]
        return red, green, blue

    def merge_channels(self, red, green, blue):
        return np.stack([red, green, blue], axis=2)

    def channel_adjust(self, channel, values):
        orig_size = channel.shape
        flat_channel = channel.flatten()
        adjusted = np.interp(flat_channel, np.linspace(0, 1, len(values)), values)
        return adjusted.reshape(orig_size)

    def activate_filters(self, text):
        original_image = skimage.img_as_float(io.imread(self.filename))
        if text == 'Original':
            self.image.setPixmap(QtGui.QPixmap(self.filename))
        if text == 'Sharped':
            sharper = self.sharpen(original_image, 1.3, .3)
            self.update_image(sharper, 'Sharped')
        if text == 'Blurred':
            blurred = self.sharpen(original_image, 0, -1.)
            self.update_image(blurred, 'Blurred')
        if text == 'Gotham':
            r, g, b = self.split_image_into_channels(original_image)
            r_boost_lower = self.channel_adjust(r, [0, 0.05, 0.1, 0.2, 0.3, 0.5, 0.7, 0.8, 0.9, 0.95, 1.0])
            bluer_blacks = self.merge_channels(r_boost_lower, g, np.clip(b + 0.03, 0, 1.0))

            gotham = self.sharpen(bluer_blacks, 1.3, .3)
            b = gotham[:, :, 2]
            b_adjusted = self.channel_adjust(b, [0, 0.047, 0.118, 0.251, 0.318, 0.392, 0.42, 0.439, 0.475,
                                                 0.561, 0.58, 0.627, 0.671, 0.733, 0.847, 0.925, 1])
            gotham[:, :, 2] = b_adjusted
            self.update_image(gotham, 'Gotham')

    def sharpen(self, image, a, b, sigma=10):
        blurred = filters.gaussian(image, sigma=sigma, multichannel=True)
        sharper = np.clip(image * a - blurred * b, 0, 1.)
        return sharper

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    ip = InstagramProcess()
    sys.exit(app.exec_())
