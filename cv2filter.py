import os
import sys

import cv2
import numpy as np
from PyQt5 import QtWidgets, QtGui, QtCore


class CV2Filters(QtWidgets.QWidget):
    def __init__(self):
        super(CV2Filters, self).__init__()

        self.filename = ''
        self.image = QtWidgets.QLabel()
        self.open_btn = QtWidgets.QPushButton('Open image...')
        self.original_im_btn = QtWidgets.QPushButton('Original image')

        self.d_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal, self)
        self.d_slider.setMinimum(0)
        self.d_slider.setMaximum(20)
        self.d_slider.setEnabled(False)
        self.d_slider.setTickPosition(QtWidgets.QSlider.TicksAbove)
        self.d_slider_label = QtWidgets.QLabel('Diameter of each pixel neighborhood')

        # self.sigmacolor_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal, self)
        # self.sigmacolor_slider.setMinimum(0)
        # self.sigmacolor_slider.setMaximum(100)
        # self.sigmacolor_slider.setEnabled(False)
        # self.sigmacolor_slider.setSliderPosition(75)
        # self.sigmacolor_slider.setTickPosition(QtWidgets.QSlider.TicksAbove)
        # self.sigmacolor_slider_label = QtWidgets.QLabel('Filter sigma in the color space')

        self.ui()
        self.setup()

    def ui(self):
        main_layout = QtWidgets.QVBoxLayout()
        button_layout = QtWidgets.QVBoxLayout()

        push_buttons_layout = QtWidgets.QHBoxLayout()
        push_buttons_layout.addWidget(self.open_btn)
        push_buttons_layout.addWidget(self.original_im_btn)

        button_layout.addLayout(push_buttons_layout)
        button_layout.addWidget(self.d_slider_label)
        button_layout.addWidget(self.d_slider)

        main_layout.addLayout(button_layout)
        main_layout.addWidget(self.image)

        self.setLayout(main_layout)
        self.show()
        self.setMinimumSize(500, 500)
        self.move(QtWidgets.QApplication.desktop().screen().rect().center() - self.rect().center())

    def setup(self):
        self.open_btn.clicked[bool].connect(lambda: self.open_image())
        self.original_im_btn.clicked[bool].connect(lambda: self.set_original_image())
        self.d_slider.valueChanged[int].connect(lambda value: self.valuechanged(value))

    def valuechanged(self, d, sigmaColor=75, sigmaSpace=75):
        if d == 0:
            self.image.setPixmap(QtGui.QPixmap(self.filename))
            return

        blur = cv2.bilateralFilter(self.original_image, d, sigmaColor, sigmaSpace)
        self.update_image(blur, '{}{}{}'.format(d, sigmaColor, sigmaSpace))

    def set_original_image(self):
        self.image.setPixmap(QtGui.QPixmap(self.filename))

    def open_image(self):
        self.filename = QtWidgets.QFileDialog.getOpenFileName(self,
                                                              'Open...',
                                                              os.path.expanduser('~'),
                                                              'Image files (*.jpg *.png)')[0]
        self.image.setPixmap(QtGui.QPixmap(self.filename))
        self.original_image = cv2.imread(self.filename)
        self.d_slider.setEnabled(True)

    def update_image(self, image, name):
        new_filename = self.make_newfilename(name)
        cv2.imwrite(new_filename, image)
        self.image.setPixmap(QtGui.QPixmap(new_filename))
        os.remove(new_filename)

    def make_newfilename(self, name):
        new_filename = self.filename.split('/')
        new_filename[-1] = '.{0}{1}'.format(name.lower(), new_filename[-1])
        return '/'.join(new_filename)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    ip = CV2Filters()
    sys.exit(app.exec_())
